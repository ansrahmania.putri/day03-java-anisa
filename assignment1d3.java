package arrays;

import java.util.Scanner;

public class assignment1d3 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        //input panjang array
        System.out.print("Panjang array adalah :");
        int inputNumber = input.nextInt();

        //panjang array
        int[] number = new int[inputNumber];

        //masukkan nilai tiap index di array
        for (int i = 0; i < number.length; i++) {
            System.out.print("Masukkan index ke-"+ i +": ");
            number[i] = input.nextInt();
        }

        //print array number
        System.out.print("Array number adalah : [");
        for (int el: number) {
            System.out.print(" "+el+" ");
        }
        System.out.println("]");

        input.close();

    }
}