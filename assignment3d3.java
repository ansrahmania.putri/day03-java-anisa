package arrays;

import java.util.Scanner;

public class assignment3d3 {
    public static void binarySearch(int[] a, int target) {
        int left = 0;
        int middle;
        int right = a.length - 1;
        while (left <= right) {
            middle = (left + right) / 2;
            if (a[middle] == target) {
                System.out.println("Element found at index " + middle);
                break;
            } else if (a[middle] < target) {
                left = middle + 1;
            } else if (a[middle] > target) {
                right = middle - 1;
            }
        }
    }

    // method bubble sort
    public static int[] bubbleSort(int[] a) {
        for (int i = 0; i < a.length - 1; i++) {
            for (int j = 0; j < a.length - 1 - i; j++) {
                // System.out.println(a[j + 1] = + a[j + 1]);
                // System.out.println(a[j] = + a[j]);
                if (a[j + 1] < a[j]) {
                    int temp = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = temp;
                }
            }
        }
        return a;
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int choices = 0;

        System.out.println("Masukkan jumlah data pada array: ");
        int inputNumber = input.nextInt();
        int[] number = new int[inputNumber];

        do {
            System.out.println("Menu");
            System.out.println("1. Input data");
            System.out.println("2. Sortir data");
            System.out.println("3. Binary Search");
            System.out.println("4. EXIT");
            System.out.println("Input nomor (1/2/3/4)");
            choices = input.nextInt();

            switch (choices) {
                case 1:

                    for (int i = 0; i < number.length; i++) {
                        System.out.print("Masukkan index ke-"+ i +": ");
                        number[i] = input.nextInt();
                    }

                    System.out.print("Array number adalah : [");
                    for (int el: number) {
                        System.out.print(" "+el+" ");
                    }
                    System.out.println("]");
                    continue;
                case 2:
                    bubbleSort(number);

                    System.out.print("Array number adalah : [");
                    for (int el: number) {
                        System.out.print(" "+el+" ");
                    }
                    System.out.println("]");
                    break;
                case 3:
                    System.out.println("Input angka yang ingin dicari: ");
                    int target = input.nextInt();

                    binarySearch(number, target);
                    break;
                default:
                    if (choices == 4) {
                        break;
                    } else {
                        System.out.println("Menu tidak ditemukan\n");
                    }
                    }

        } while (choices != 4);
    }
}
