package arrays;

import java.util.*;

class assignment5d3 {
    public static void main(String[] args) {
        ArrayList<String> al= new ArrayList<String>();

        al.add("Peter");
        al.add("John");
        al.add("Billy");

        Iterator itr=al.iterator();
        while(itr.hasNext()){
            System.out.println(itr.next());
        }

        System.out.println("---------------");

        al.add("Jack");

        Iterator itr2=al.iterator();
        while(itr2.hasNext()){
            System.out.println(itr2.next());
        }

        System.out.println("---------------");

        al.remove("Peter");

        Iterator itr3=al.iterator();
        while(itr3.hasNext()){
            System.out.println(itr3.next());
        }

        System.out.println("---------------");

        ArrayList<String> retainAl= new ArrayList<String>();

        retainAl.add("John");
        retainAl.add("Jack");

        Iterator itr4=retainAl.iterator();
        while(itr4.hasNext()){
            System.out.println(itr4.next());
        }

    }

}






