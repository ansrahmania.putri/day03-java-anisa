import java.util.Scanner;

public class assignment2d3 {

    public static void main(String args[]){
        Scanner input = new Scanner(System.in);

        //input baris array
        System.out.print("Baris array adalah :");
        int inputBaris = input.nextInt();

        //input kolom array
        System.out.print("Kolom array adalah :");
        int inputKolom = input.nextInt();

        //inisialisasi array
        int[][] number = new int[inputBaris][inputKolom];

        //masukkan nilai tiap index di array
        for (int i = 0; i < number.length; i++) {
            for (int j = 0; j < number[i].length; j++) {
                System.out.print("Masukkan baris ke- ["+ i +"] kolom ke- ["+ j+ "] :");
                number[i][j] = input.nextInt();
            }
        }

        // print baris dan kolom array
                System.out.println("\nBaris dan Kolom Array 2 Dimensi adalah ["+inputBaris+","+inputKolom+"]");

        // print isi array
        System.out.print("Array Multidimensi 2 Dimensi\n");
        for (int i = 0; i < number.length; ++i) {
            for(int j = 0; j < number[i].length; ++j) {
                System.out.print(number[i][j]+" ");
            }
            System.out.println();
        }

        input.close();
    }
}
